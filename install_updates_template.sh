#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

PREFIX_PATH=PREFIXPATH
LOCAL_BIN=$SCRIPT_DIR/ARCHPREFIX-care-gw.tar.xz
export PREFIX_PATH
export SCRIPT_DIR
$SCRIPT_DIR/get_bin.sh $LOCAL_BIN
GET_SUCCESS=$?

if [[ GET_SUCCESS -eq 0 ]]; then
    systemctl enable care.gateway.service
    systemctl restart care.gateway.service
fi