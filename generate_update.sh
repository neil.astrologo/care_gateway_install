#!/bin/bash

echo "Retrieving command line arguments from care.gateway.service"
MAIN_SERVICE=/etc/systemd/system/care.gateway.service

# Read the ExecStart line from the service file
exec_start=$(grep '^ExecStart=' $MAIN_SERVICE)

# Extract the command line arguments after 'painlessMeshBoost'
arguments=${exec_start#*painlessMeshBoost }

source parse_args.sh $arguments

echo "Argument after -n: $NODEID"
echo "Argument after -i: $GATEWAY_ID"
echo "Argument after -j: $DATA_PREFIX"
echo "Mode: $MESH_MODE"
echo "Prefix: $PREFIX_PATH"

# generate care.update.service and care.update.timer
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
UPDATE_TEMPLATE=$SCRIPT_DIR/care.update.service.template
UPDATE_SERVICE=/etc/systemd/system/care.update.service

sed -e "s|PREFIX_PATH|$PREFIX_PATH|g" -e "s|NODEID|$NODEID|g" -e "s|GATEWAY_ID|$GATEWAY_ID|g" -e "s|DATA_PREFIX|$DATA_PREFIX|g" -e "s|MESH_MODE|$MESH_MODE|g" "$UPDATE_TEMPLATE" > "$UPDATE_SERVICE"

# copy timer template to systemd
cp $SCRIPT_DIR/care.update.timer.template /etc/systemd/system/care.update.timer
systemctl daemon-reload
systemctl enable care.update.timer
systemctl start care.update.timer