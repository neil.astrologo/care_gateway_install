#!/bin/bash

# get own MAC address to use as the default gateway id
mac_address=$(ip a show wlan0 | awk '/ether/ {print $2}' | sed 's/://g')
mac_address_upper=$(echo $mac_address | awk '{print toupper($0)}')
id_prefix=""

# get runtime directory
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
echo $SCRIPT_DIR

# Set default values for optional arguments
GATEWAY_ID_D=$id_prefix$mac_address_upper
DATA_PREFIX_D="UPCARE/v2/EMB_NCR"
PREFIX_PATH_D="$HOME"
MESH_MODE_D="s"
NODEID_D=6969
BRANCH_D="aws"

# assign default values to variables that will be used
GATEWAY_ID=$GATEWAY_ID_D
DATA_PREFIX=$DATA_PREFIX_D
PREFIX_PATH=$PREFIX_PATH_D
MESH_MODE=$MESH_MODE_D
NODEID=$NODEID_D
BRANCH=$BRANCH_D
LOCAL=0

# Usage guide function
function usage {
  echo "Usage: $0 [-i <id>] [-d <data_prefix>] [-p <path_prefix>] [-m <mode>] [-n <nodeid>] [-b <branch>] [-l <local_bin>]"
  echo "  -i <id>           The required gateway ID argument (default: '$GATEWAY_ID_D', aka GW_<MAC_ADDRESS>)"
  echo "  -d <data_prefix>  The prefix needed for the upstream data topic (default: '$DATA_PREFIX_D')"
  echo "  -p <path_prefix>  Optional prefix path argument (default: '$PREFIX_PATH_D')"
  echo "  -m <mode>         Optional mesh mode argument ('server'/'s' or 'client'/'c', default: '$MESH_MODE_D')"
  echo "  -n <nodeid>       Optional integer argument for node ID (default: '$NODEID_D')"
  echo "  -b <branch>       Optional branch argument (default: '$BRANCH_D')"
  echo "  -l <local_bin>    Use local binaries located in local_bin instead of retrieving from branch"
  echo "  Note: -l and -b are mutually exclusive."
}

if [[ "$1" == "--help" || "$1" == "-h" ]]; then
  usage
  exit 0
fi

# Parse arguments
while getopts ":i:d:p:m:n:b:l:" opt; do
  case $opt in
    i)
      GATEWAY_ID="$OPTARG"
      ;;
    d)
      DATA_PREFIX="$OPTARG"
      ;;
    p)
      PREFIX_PATH="$OPTARG"
      ;;
    m)
      case "$OPTARG" in
        s|server)
          MESH_MODE="s"
          ;;
        c|client)
          MESH_MODE="c"
          ;;
        *)
          echo "Invalid mode argument. Please use 'server'/'s' or 'client'/'c'." >&2
          usage
          exit 1
          ;;
      esac
      ;;
    n)
      if ! [[ "$OPTARG" =~ ^[0-9]+$ ]]; then
        echo "Node ID must be an integer." >&2
        usage
        exit 1
      fi
      NODEID="$OPTARG"
      ;;
    b)
      if [[ $LOCAL -eq 1 ]]; then
        echo "Cannot use -b option with -l option."
        usage
        exit 1
      else
        BRANCH="$OPTARG"
      fi
      ;;
    l)
      if [[ $BRANCH != $BRANCH_D ]]; then
        echo "Cannot use -l option with -b option."
        usage
        exit 1
      else
        LOCAL=1
        LOCAL_BIN="$OPTARG"
      fi
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      exit 1
      ;;
  esac
done

# Check if PREFIX_PATH is valid
if [[ ! -d "$PREFIX_PATH" ]]; then
  echo "Invalid prefix path: $PREFIX_PATH" >&2
  usage
  exit 1
fi

# Print arguments
echo "GATEWAY_ID: $GATEWAY_ID"
echo "DATA_PREFIX: $DATA_PREFIX"
echo "PREFIX_PATH: $PREFIX_PATH"
echo "MESH_MODE: $MESH_MODE"
echo "NODEID: $NODEID"
if [[ $LOCAL -ne 1 ]]; then
  echo "BRANCH: $BRANCH"
else
  echo "LOCAL_TARFILE: $LOCAL_BIN"
fi

if [ $LOCAL -ne 1 ]; then
    export PREFIX_PATH
    export SCRIPT_DIR
    echo "Retrieving binaries from the branch $BRANCH..."
    if [ $BRANCH == "aws" ]; then
        $SCRIPT_DIR/get_bin.sh
    else
        $SCRIPT_DIR/get_bin.sh testtoken $BRANCH
    fi

    # check if get_bin was successful
    if [[ $? -eq 1 ]]; then
        echo "Artifact retrieval failed"
        exit 1
    fi

    echo "Artifact retrieval and extraction successful"
else
    # check if tarfile is valid
    if [ ! -f "$LOCAL_BIN" ]; then
        echo "$LOCAL_BIN does not exist"
        exit 1
    else
        # check if file is a valid .tar.xz file
        if [[ $(file -b "$LOCAL_BIN") == *"XZ compressed data"* ]]; then
            echo "$LOCAL_BIN is a .tar.xz file. Proceeding with extraction."
            export PREFIX_PATH
            export SCRIPT_DIR
            $SCRIPT_DIR/get_bin.sh $LOCAL_BIN
        else
            echo "$LOCAL_BIN is not a .tar.xz file."
            exit 1
        fi
    fi
fi

echo "Creating service file..."

# Create the service file
SERVICE_TEMPLATE="$SCRIPT_DIR/care.gateway.service.template"
SERVICE_FILE="/etc/systemd/system/care.gateway.service"

sed -e "s|PREFIXPATH|$PREFIX_PATH|g" -e "s|NODEID|$NODEID|g" -e "s|GATEWAY_ID|$GATEWAY_ID|g" -e "s|DATA_PREFIX|$DATA_PREFIX|g" -e "s|MESH_MODE|$MESH_MODE|g" "$SERVICE_TEMPLATE" > "$SERVICE_FILE"

echo "Service file created."
echo "Creating global.conf..."

# Create the log .conf file
LOGCONF_TEMPLATE="$SCRIPT_DIR/conf/global.conf.template"
LOGCONF_FILE="$PREFIX_PATH/CARE/painlessmeshboost/conf/global.conf"
sed "s|PREFIXPATH|$PREFIX_PATH|g" "$LOGCONF_TEMPLATE" > "$LOGCONF_FILE"

echo "global.conf created."

# Create the credentials.txt file if it does not yet exist
CREDS_TEMPLATE="$SCRIPT_DIR/conf/credentials.txt.template"
CREDS_FILE="$PREFIX_PATH/CARE/painlessmeshboost/conf/credentials.txt"

if [ ! -f "$CREDS_FILE" ]; then
    cp "$CREDS_TEMPLATE" "$CREDS_FILE"
else
    echo "$CREDS_FILE already exists. Will not generate a new one."
fi

echo "Cleaning up template files"
rm $PREFIX_PATH/CARE/painlessmeshboost/conf/*.template
rm $PREFIX_PATH/CARE/painlessmeshboost/*.template

# give ownership of CARE to current user
chown -R $SUDO_USER:$SUDO_USER "$PREFIX_PATH/CARE"

ARCHPREFIX=""

# Check architecture
ARCH=$(uname -m)
if [[ "$ARCH" == "armv7l" || "$ARCH" == "armv6l" ]]; then
  ARCHPREFIX="armv6"
elif [[ "$ARCH" == "aarch64" ]]; then
  ARCHPREFIX="aarch64"
else
  echo "Unsupported architecture: $ARCH"
  exit 1
fi

# IN HERE: MODIFY UPDATER SERVICE/SCRIPT TEMPLATES
# check if care.updater.service already exists
SCRIPT_TEMPLATE="$SCRIPT_DIR/check_updates_template.sh"
SCRIPT_UPDATER="$SCRIPT_DIR/check_updates.sh"
UPDATER_TEMPLATE="$SCRIPT_DIR/care.update.service.template"
UPDATER_SERVICE="/etc/systemd/system/care.update.service"

echo "Generating update script"
sed -e "s|PREFIXPATH|$PREFIX_PATH|g" -e "s|NODEID|$NODEID|g" -e "s|GATEWAY_ID|$GATEWAY_ID|g" -e "s|DATA_PREFIX|$DATA_PREFIX|g" -e "s|MESH_MODE|$MESH_MODE|g" -e "s|ARCHPREFIX|$ARCHPREFIX|g" -e "s|SCRIPTDIR|$SCRIPT_DIR|g" "$SCRIPT_TEMPLATE" > "$SCRIPT_UPDATER"
chmod +x $SCRIPT_UPDATER
echo "Generating updater service and timer"
sed -e "s|PREFIXPATH|$PREFIX_PATH|g" -e "s|NODEID|$NODEID|g" -e "s|GATEWAY_ID|$GATEWAY_ID|g" -e "s|DATA_PREFIX|$DATA_PREFIX|g" -e "s|MESH_MODE|$MESH_MODE|g" -e "s|ARCHPREFIX|$ARCHPREFIX|g" -e "s|SCRIPTDIR|$SCRIPT_DIR|g" "$UPDATER_TEMPLATE" > "$UPDATER_SERVICE"
cp $SCRIPT_DIR/care.update.timer.template /etc/systemd/system/care.update.timer

# create config file
echo "GATEWAY_ID=$GATEWAY_ID" > install.conf
echo "DATA_PREFIX=$DATA_PREFIX" >> install.conf
echo "PREFIX_PATH=$PREFIX_PATH" >> install.conf
echo "MESH_MODE=$MESH_MODE" >> install.conf
echo "NODEID=$NODEID" >> install.conf

# create fileservers.txt
cp $SCRIPT_DIR/conf/fileservers.txt.default $PREFIX_PATH/CARE/painlessmeshboost/conf/fileservers.txt

# create install_updates.sh
INSTALL_UPDATE_TEMPLATE="$SCRIPT_DIR/install_updates_template.sh"
INSTALL_UPDATE_SCRIPT="$SCRIPT_DIR/install_updates.sh"
sed -e "s|PREFIXPATH|$PREFIX_PATH|g" -e "s|NODEID|$NODEID|g" -e "s|GATEWAY_ID|$GATEWAY_ID|g" -e "s|DATA_PREFIX|$DATA_PREFIX|g" -e "s|MESH_MODE|$MESH_MODE|g"  -e "s|ARCHPREFIX|$ARCHPREFIX|g" "$INSTALL_UPDATE_TEMPLATE" > "$INSTALL_UPDATE_SCRIPT"
chmod +x $INSTALL_UPDATE_SCRIPT

# copy timer to systemd
cp $SCRIPT_DIR/care.update.timer.template /etc/systemd/system/care.update.timer
cp $SCRIPT_DIR/monitor $PREFIX_PATH
# Reload the systemd configuration and start the service
systemctl daemon-reload 
systemctl enable care.update.timer
systemctl restart care.update.timer
systemctl enable care.gateway.service
systemctl restart care.gateway.service
