#!/bin/bash

PREFIX_PATH=PREFIXPATH
# get script directory
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# retrieve executable if needed
$SCRIPT_DIR/bin/ARCHPREFIX-care-gw-updater -p $PREFIX_PATH

if [[ $? -eq 1 ]]; then
    $SCRIPT_DIR/install_updates.sh
    exit 0
else
    exit 0
fi