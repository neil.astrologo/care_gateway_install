#!/bin/bash

# declare defaults
DEFAULT_BASE_URL="https://sync.upcare.ph"
DEFAULT_FW_ROUTE="api/firmware"

function get_scriptdir {
    echo "$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
}

# create global SCRIPT_DIR variable
SCRIPT_DIR=$(get_scriptdir)

function get_mac {
    local mac=$(ip a show wlan0 | awk '/ether/ {print $2}' | sed 's/://g' | awk '{print toupper($0)}')

    echo $mac
}

function get_arch {
    local arch=$(uname -m)

    [[ $arch == "armv7l" || $arch == "armv6l" ]] && echo "armv6" && return 0
    [[ $arch == "aarch64" ]] && echo "aarch64" && return 0
    [[ $arch == "x86_64" ]] && echo "x86_64" && return 0
    echo "Unsupported architecture: $arch" && return 1
}

function decompose_version(){
    local version=$1

    IFS='.-' read -r -a array <<< $version

    echo ${array[@]}
}

function compare_version(){
    local old=$1 new=$2

    local old_vers=($(decompose_version $old))
    local new_vers=($(decompose_version $new))
    local length=${#old_vers[@]}

    for ((i=0; i<length; i++)); do
        [[ ${old_vers[i]} -gt ${new_vers[i]} ]] && return 0
    done

    return 1
}

function get_projdir(){
    local prefixdir=$1 projname=$2

    local projdir=$prefixdir/CARE/$projname

    echo $projdir
}

function check_and_create_dir(){
    local dir=$1

    if [[ ! -d $dir ]]; then
        echo "Creating directory $dir..."
        mkdir -p $dir
        [[ $? -eq 0 ]] && echo "Directory creation successful" && return 0
        echo "Directory creation failed" && return 1
    fi
}

function check_package {
    local build=$1

    # create subterminal
    (
        cd $SCRIPT_DIR
        local success=$(md5sum -c $build.sh.md5 | awk '{print $2}')
        [[ $success == "OK" ]] && return 0
        return 1
    )
}

function do_curl(){
    local req=$1 url=$2 header=$3 output=$4

    curl -s --request $req --url $url --header $header
}

function parse_arch_url(){
    local arch url

    # retrieve arguments -a and -u
    while getopts ":a:u:" opt; do
        case $opt in
            a)
                arch=$OPTARG
                ;;
            u)
                url=$OPTARG
                ;;
        esac
    done

    [[ -z $arch ]] && arch=$(get_arch)
    [[ -z $url ]] && url=$DEFAULT_BASE_URL/$DEFAULT_FW_ROUTE
    
    # pack arch and url into an array
    local retval=($arch $url)
    echo "${retval[@]}"
}

function get_version(){
    local arch base_url
    # Parse options
    local retval=($(parse_arch_url "$@"))

    arch=${retval[0]}
    base_url=${retval[1]}

    local full_url=$base_url/$arch-version.txt

    local version=$(do_curl GET "$full_url" 'accept: file/*')
    echo $version
}

function get_package(){
    local arch base_url
    # Parse options
    local retval=($(parse_arch_url "$@"))

    arch=${retval[0]}
    base_url=${retval[1]}

    version=$(get_version -a $arch -u $base_url)
    version_short=${version%-*}

    local success=1 retry_ctr=0

    local build=$arch-care-gw-$version_short
    local url=$base_url/$build

    while [[ $success -eq 1 && $retry_ctr -lt 10 ]]; do
        curl --request GET --url "$url.sh" --header 'accept: file/*' --output $SCRIPT_DIR/$build.sh
        curl --request GET --url "$url.sh.md5" --header 'accept: file/*' --output $SCRIPT_DIR/$build.sh.md5
        check_package $build
        success=$?
        retry_ctr=$((retry_ctr+1))
    done
    
    if [[ $success -eq 0 ]]; then
        chmod +x $SCRIPT_DIR/$build.sh
        echo "$SCRIPT_DIR/$build.sh"
        return 0
    else
        echo "Failed to download package"
        return 1
    fi
}

function write_default_service_file {
    local service_file_path="/etc/systemd/system/care.gateway.service"

    cat <<EOF > $service_file_path
[Unit]
Description=CARE Gateway for ESP-based Network
Requires=influxd.service network-online.target
After=influxd.service network-online.target start-softap.service

[Service]
User=pi
Restart=always
RestartSec=5
ExecStart=painlessmeshboost -s

[Install]
WantedBy=multi-user.target
EOF
}

function write_default_launch_params(){
    local prefixdir=$1 projname=$2

    local projdir=$(get_projdir $prefixdir $projname)
    local launchparamspath=$projdir/conf/launch_params.json
    
    local default_clientid=GW_$(get_mac)
    local default_dataprefix=UPCARE/PROJECT1/OUTDOOR/GW

cat <<EOF > $launchparamspath
{
    "clientid": "$default_clientid",
    "dataprefix": "$default_dataprefix",
    "logLevel": [],
    "nodeId": 6969,
    "otaDir": "",
    "performance": 2.0,
    "port": 5555
}
EOF
}

function write_default_logger_conf(){
    local prefixdir=$1 projname=$2

    local projdir=$(get_projdir $prefixdir $projname)
    local logconfdir=$projdir/conf
    local logconfpath=$logconfdir/global.conf

    check_and_create_dir $logconfdir

cat <<EOF > $logconfpath
-- default
* GLOBAL:
    FORMAT = "[%datetime{%Y-%b-%d %H:%m:%s}][%thread_name][%level] : %msg"
    TO_FILE = true
    FILENAME = "$prefixdir/CARE/$projname/logs/logs_%datetime{%Y%b%d}.log"
    LOG_FLUSH_THRESHOLD = 5
    To_Standard_Output = true
EOF
}

function create_exec_symlink(){
    local prefixdir=$1 projname=$2 execname=$3
    local projdir=$(get_projdir $prefixdir $projname)

    local fullexecpath=$projdir/bin/$execname
    local symlinkpath=/usr/local/bin/$execname

    ln -s $fullexecpath $symlinkpath
}

function parse_args(){
    local arg_len=${#}
    
    [[ $arg_len -eq 0 ]] && echo "Please provide arguments" && exit 0

    # create defaults for some variables
    local default_gatewaymode=s default_clientid="GW_$(get_mac)" default_dataprefix="UPCARE/PROJECT1/OUTDOOR/GW"

    # variables for switches
    declare -g MODESW=0 GETPACKAGESW=0 CREATECFGSW=0 PREFIXDIRSW=0 prefixdir DATAPREFIXSW=0 dataprefix CUSTOMURLSW=0 url GATEWAYMODESW=0 gatewaymode CLIENTIDSW clientid CUSTOMARCHSW=0 customarch

    while getopts ":FUCp:d:u:m:i:a:" opt; do
        case $opt in
            F)
                MODESW=1
                GETPACKAGESW=1
                CREATECFGSW=1
                ;;
            U)
                MODESW=1
                GETPACKAGESW=1
                CREATECFGSW=0
                ;;
            C)
                MODESW=1
                GETPACKAGESW=0
                CREATECFGSW=1
                ;;
            p)
                PREFIXDIRSW=1
                prefixdir=$OPTARG
                ;;
            d)
                DATAPREFIXSW=1
                dataprefix=$OPTARG
                ;;
            u)
                CUSTOMURLSW=1
                customurl=$OPTARG
                ;;
            m)
                GATEWAYMODESW=1
                gatewaymode=$OPTARG
                ;;
            i)
                CLIENTIDSW=1
                clientid=$OPTARG
                ;;
            a)
                CUSTOMARCHSW=1
                customarch=$OPTARG
                ;;
            \?)
                echo "Invalid option: $OPTARG" >&2
                usage
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument" >&2
                usage
                exit 1
                ;;
        esac
    done

    # check for required switches
    [[ $MODESW -eq 0 ]] && echo "Specify if install mode is full or update" && exit 1
    [[ $PREFIXDIRSW -eq 0 ]] && echo "Please specify the prefix directory" && exit 1
}

function usage {
    echo "Usage: $0 (-F|-U|-C) -p PREFIX [-u CUSTOM_PACKAGE_URL] [-a CUSTOM_ARCH] [-d DATA_TOPIC_PREFIX] [-i CUSTOM_CLIENTID] [-m GATEWAY_MODE]"
    echo "  -F                      Runs the install script in full install mode, where binaries and libraries are downloaded and config files are generated."
    echo "  -U                      Runs the install script in update mode. Binaries and libraries are downloaded, but no config files are written."
    echo "  -C                      Runs the install script in config mode. Binaries and libraries are not downloaded, and config files are generated."
    echo "  -p PREFIX               Specifies the prefix directory for installation."
    echo "  -u CUSTOM_PACKAGE_URL   Overrides the default location (https://sync.upcare.ph) of binaries"
    echo "  -a CUSTOM_ARCH          Specifies the architecture of binaries to be retrieved. Valid values are 'armv6' or 'aarch64'."
    echo "  -d DATA_TOPIC_PREFIX    Overrides the default prefix of the MQTT topic (UPCARE/v2/PROJECT1/GW) being used for data publish."
    echo "  -i CUSTOM_CLIENTID      Overrides the default clientid (MAC address without the first four octets) that will be appended to the MQTT topic prefix to form the whole topic."
    echo "  -m GATEWAY_MODE         Specifies the mode the gateway will use in painlessMeshBoost operation (default is 's'). Valid varies are 's' or 'c'."
}

function main(){
    [[ $1 == "--help" || $1 == "-h" ]] && usage && exit 0

    parse_args $@

    local url arch

    if [[ $GETPACKAGESW -eq 1 ]]; then
        [[ $CUSTOMURLSW -eq 1 ]] && url=$customurl || url=$DEFAULT_BASE_URL/$DEFAULT_FW_ROUTE
        [[ $CUSTOMARCHSW -eq 1 ]] && arch=$customarch || arch=$(get_arch)

        install_script=$(get_package -a arch -u url)
        success_download=$?

        echo $success_download
        echo $install_script


        if [[ $success_download -eq 0 ]]; then
            check_and_create_dir $prefixdir
            /bin/bash $install_script --skip-license --exclude-dir --prefix=$prefixdir
        else
            exit 1
        fi
    fi

    if [[ $CREATECFGSW -eq 1 ]]; then
        write_default_service_file
        write_default_launch_params
        write_default_logger_conf
    fi
}

main $@
