#!/bin/bash

# check if PREFIX_PATH environment variable is set
if [[ -z "$PREFIX_PATH" ]]; then
    echo "Error: PREFIX_PATH environment variable not set"
    exit 1
fi

AWSPREFIX=""
AWSBUILD="care-gw.tar.xz"
VERSIONTEXT="version.txt"

# Check architecture
ARCH=$(uname -m)
if [[ "$ARCH" == "armv7l" || "$ARCH" == "armv6l" ]]; then
  BUILD="package-armv6"
  AWSPREFIX="armv6-"
elif [[ "$ARCH" == "aarch64" ]]; then
  BUILD="package-aarch64"
  AWSPREFIX="aarch64-"
else
  echo "Unsupported architecture: $ARCH"
  exit 1
fi

AWSBUILD=$AWSPREFIX$AWSBUILD
VERSIONTEXT=$AWSPREFIX$VERSIONTEXT

if [[ $# -eq 0 ]]; then
    curl --request GET --url "https://sync.upcare.ph/api/firmware/""$AWSBUILD" --header 'accept: file/*' --output $SCRIPT_DIR/artifacts.tar.xz
    echo "artifacts.tar.xz downlaoded successfully."
    echo "extracting to $PREFIX_PATH"
    mkdir -p $PREFIX_PATH && tar xJf $SCRIPT_DIR/artifacts.tar.xz -C $PREFIX_PATH
    rm $SCRIPT_DIR/artifacts.tar.xz
    exit 0
fi

# if there is one argument, assume that it is a .tar.xz file
# the checks are done in the install.sh parent script anyway, probably no need to check here
if [[ $# -eq 1 ]]; then
    echo "Extracting $1 to $PREFIX_PATH"
    mkdir -p $PREFIX_PATH && tar xJf $1 -C $PREFIX_PATH

    if [[ $? -eq 0 ]]; then
        echo "Extraction successful"
        exit 0
    else
        echo "Something went wrong..."
        exit 1
    fi
fi

# check number of arguments, should be 2
if [[ $# -ne 2 ]]
    then echo "Two arguments should be provided: private token and branch name"
    else
        if [[ -f $1 ]]
        then
            # input contents of token file to variable
            gitlab_token=`cat $1`
            curl --location --output artifacts.zip --header "PRIVATE-TOKEN: ""$gitlab_token""" "https://gitlab.com/api/v4/projects/36690178/jobs/artifacts/""$2""/download?job=""$BUILD"
            
            # check if artifacts.zip is an actual zip file
            string=`file artifacts.zip`
            if [[ $string == *"JSON"* ]]
            then
                success=0
                exit 1
            else
                success=1
            fi
        else
            echo "Specify file that contains token."
        fi

    if [[ $success -eq 1 ]]
    then
        echo "artifacts.zip downloaded successfully"
        echo "Extrating to $PREFIX_PATH"
        # extract and install contents of artifacts.zip
        unzip -d "$PREFIX_PATH" artifacts.zip
    else
        echo `cat artifacts.zip`
        rm artifacts.zip
    fi
fi
