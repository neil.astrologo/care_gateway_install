#!/bin/bash

# get mac address
mac_address=$(ip a show wlan0 | awk '/ether/ {print $2}' | sed 's/://g')
mac_address_upper=$(echo $mac_address | awk '{print toupper($0)}')
id_prefix="GW_"

# set defaults
MESH_MODE=c
PREFIX_PATH="$HOME"
NODEID=6969
DATA_PREFIX="jsontest"
GATEWAY_ID=$id_prefix$mac_address_upper


while getopts "n:i:j:cs" opt; do
  case $opt in
    n) NODEID="$OPTARG";;
    i) GATEWAY_ID="$OPTARG";;
    j) DATA_PREFIX="$OPTARG";;
    c) MESH_MODE="c";;
    s) MESH_MODE="s";;
    \?) echo "Invalid option: -$OPTARG";;
  esac
done